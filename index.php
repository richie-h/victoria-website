<!DOCTYPE html>
<html>
    <head>  
        <meta charset="UTF-8">
        <meta name="description" content=""/>        
        <link rel="author" href="https://google.com/+VictoriaPierceBeauty"/>

        <!-- Social Media -->
        <meta property="og:title" content="Victoria Pierce"/>
        <meta property="og:type" content="website"/>
        <meta property="og:description" content=""/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

        <title>Victoria Pierce - Digital Marketing Executive - Ipswich, Suffolk</title>

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> 
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>

    <!-- Add Microdata tags to the entire web page, in this case we are using the Person itemtype -->
    <body itemscope itemtype="http://schema.org/Person">
        <header>
            <div class="container">
                <div class="page-header">
                    <div class="row" >                      
                            
                            <div class="col-md-8">
                                <h1><span itemprop="name">Victoria Pierce</span></h1>
                                <h3><span itemprop="jobTitle">Digital Marketing Executive</span></h3><br />
                                 <div class="col-md-8">
                        <blockquote>
    <p>All Lasting Business is Built on Friendship</p>
    <footer>Alfred A Montapert</footer>
  </blockquote>
  </div>
                            </div> 
                            <div class="col-md-4">
                            <div id="logo-container">
                                <img id="logo" src="images/B+W.png" alt="Hexagon Logo"  />
                            </div>
                            </div>
                           
                    </div>
                </div>
            </div>
        </header>

        <section id="personal-profile">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Personal Profile</h2>
                        <p>
                            Victoria Pierce is a creative, friendly and professional
                            Digital Marketing Consultant based in Ipswich, Suffolk.
                            She has worked with a variety of businesses in the East
                            of England, whether they are just starting up or SMEs. 
                        </p>
                        <p>
                            Marketing is her thing, stick 'digital' in front of that
                            and she gets really excited! So, whether you want some
                            help promoting your website, are looking for someone to
                            manage your social media, or just want to chat about blogging,
                            don't hesitate to get in touch!
                        </p>   

                    </div> 
                </div>
            </div>
        </section>

        <!-- Skill cloud section -->
        <section id='skill-cloud'>
            <div class="container">
                <div class="row">
                    <hr />
                    <div class="col-md-12">
                        <h2>Skills</h2>
                        <p class='skill-cloud'>
                            <span class="cloud-experienced" title='Analysis'>Analysis</span> .
                            <span class="cloud-guru" title='Blogging'>Blogging</span> .
                            <span class="cloud-expert" title='Content Creation'>Content Creation</span> .
                            <span class="cloud-guru" title='Digital Marketing'>Digital Marketing</span> .
                            <span class="cloud-experienced" title='Email Marketing'>Email Marketing</span> .
                            <span class="cloud-work" title='Illustrator'>Illustrator</span> .
                            <span class="cloud-experienced" title='Pay Per Click'>Pay Per Click</span> .
                            <span class="cloud-work"  title='Photoshop'>Photoshop</span> .
                            <span class="cloud-experienced" title='Research'>Research</span> .
                            <span class="cloud-guru" title='SEO'>SEO</span> .
                            <span class="cloud-expert" title='Social Media Management'>Social Media Management</span> .
                            <span class="cloud-work" title='Strategy'>Strategy</span> .
                            <span class="cloud-intern" title='Web Design'>Web Design </span>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- Key Facts section of the website, using bootstrap panels -->
        <section id="key-facts">
            <div class="container">
                <div class="row">
                    <hr />
                    <div class="col-md-4">
                        <div class="panel panel-warning">
                            <h2>Increase Website Traffic</h2>
                            <p>
                                Created a digital marketing strategy for a major 
                                client in the sports industry and successfully increased
                                website traffic by 210%
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-warning">
                            <h2>Proven Mail Shot Results</h2>
                            <p>
                                Achieved over 50% open rate on a promotional email
                                to over 5,000 people. This lead to a 10% increase
                                in annual profits.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-warning">
                            <h2>Keyword Rich Content</h2>
                            <p>
                                Created keyword rich content in order to help a 
                                client reach page one organically and rank well 
                                for a specific search terms that were relevant to
                                their site.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Start the footer Section of the Web site, include a link to validate HTML5 and link to web site -->
        <footer class="footer">
            <div class="container">
                <div class="col-md-12">
                    <p class="text-muted">
                        Created by <a href="https://www.richiehughes.co.uk">Richie Hughes</a> <?php echo date("Y"); ?>
                        <a href="https://html5.validator.nu/?doc=http%3A%2F%2Fwww.victoriapierce.co.uk" target="_blank">
                            <img src="img/html5.png" class="html5-logo" alt="HTML5 Powered with CSS3 / Styling, Device Access, Performance &amp; Integration, and Semantics" title="HTML5 Powered with CSS3 / Styling, Device Access, Performance &amp; Integration, and Semantics">
                        </a>
                    </p>
                </div>
            </div>
        </footer>

    </body>
</html>